**Windows 7 Apps is a open source collection of software for Windows 7**

All softwares are available free of charge. 

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Audacity For Windows

Audacity is a free audio editor that supports Windows, Linux, and Mac. It is good for performing basic audio editing and formatting in an instinctive way that beginners can easily understand.

[Download Audocity](https://windows7hub.com/download-audacity)

---

## Google Chrome for Windows

Google Chrome lets you save your passwords and make them accessible across all your devices. If you find an interesting page that you would like to revisit, just click on ‘bookmark’ and it is saved for you.

[Download Google Chrome](https://windows7hub.com/download-google-chrome)

---

## VLC Media Player for Windows

Audacity is a free audio editor that supports Windows, Linux, and Mac. It is good for performing basic audio editing and formatting in an instinctive way that beginners can easily understand.

[Download VLC](https://windows7hub.com/download-vlc-media-player)

---

